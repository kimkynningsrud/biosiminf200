##Biological simulator modelling the ecosystem of Rossumøya

BioSim_G25_Reinholt_Kynningsrud

This is a simulation of the ecosystem of Rossumøya. Documentation can be
found in the 'docs' directory.

The simulation is working as intended, though some components have been hard to
implement due to the strict time limit and difficult nature of the project. 

Components that are lacking include:
- Histograms for visualization
- An extensive documentation
- tests for island.py and simulation.py

Developers: Vetle Reinholt, Kim Næss Kynningsrud