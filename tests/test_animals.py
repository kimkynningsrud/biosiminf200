#!python
# -*- coding: utf-8 -*-


"""
Tests for herbivores.
"""

import pytest
from pytest import approx
from biosim.animals import Herbivore, Carnivore
from biosim.simulation import BioSim
from scipy import stats
from biosim.cell import Highland, Lowland, Water, Desert


class TestAnimals:
    """
    Tests for the animals
    """

    @pytest.fixture(autouse=True)
    def create_animals(self):
        """
        Creates some animals to run the tests on.
        """
        self.herbivore = Herbivore(age=1, weight=5)
        self.new_herbivore = Herbivore()

        self.carnivore = Carnivore(age=1, weight=5)
        self.new_carnivore = Carnivore()

        self.zero_weight_herb = Herbivore(weight=0)

    def test_herbivore_class_initializer(self):
        """
        Tests that the herbivores are initialized properly.
        """
        assert self.herbivore.age == 1
        assert self.herbivore.weight == 5
        assert self.new_herbivore.age == 0
        assert self.new_herbivore.weight > 0
        assert isinstance(self.new_herbivore.age, int)
        assert isinstance(self.new_herbivore.weight, float)
        assert self.new_herbivore.fitness > 0

    def test_carnivore_class_initializer(self):
        """
        Tests that the carnivores are initialized properly
        """
        assert self.carnivore.age == 1
        assert self.carnivore.weight == 5
        assert self.new_carnivore.age == 0
        assert self.new_carnivore.weight > 0
        assert isinstance(self.new_carnivore.age, int)
        assert isinstance(self.new_carnivore.weight, float)
        assert self.new_carnivore.fitness > 0

    def test_aging(self):
        """
        Tests that the aging method increases the age of the animal by one.
        """
        for _ in range(10):
            self.new_herbivore.aging()
            self.new_carnivore.aging()
        assert self.new_herbivore.age == 10
        assert self.new_carnivore.age == 10

    def test_weight_increase(self):
        """
        Tests that the weight increase correctly for an herbivore when eating fodder.
        """
        weight_gain = self.herbivore.weight_increase(consumed=20)
        assert self.herbivore.weight < self.herbivore.weight + weight_gain
        assert weight_gain == 18

    def test_weight_decrease(self):
        """
        Tests that weight_decrease functions properly and gives the expected results.
        """
        self.herbivore.weight_decrease()
        self.carnivore.weight_decrease()
        assert self.herbivore.weight == 4.75
        assert self.carnivore.weight == 4.375

    def test_calculate_fitness(self):
        """
        Tests that calculate_fitness functions properly and gives the expected results.
        """
        assert self.herbivore.calculate_fitness() == approx(0.377386)

    def test_calculate_fitness_no_weight(self):
        """
        Tests that the value received when calculating fitness is zero, when the weight
        is set to zero
        """
        assert self.zero_weight_herb.calculate_fitness() == 0

    def test_update_fitness(self):
        """
        Tests that fitness updates properly.
        """
        assert self.herbivore.calculate_fitness() == approx(0.3773860)
        self.herbivore.weight = 15
        self.herbivore.age = 2
        self.herbivore.update_fitness()
        assert self.herbivore.fitness == approx(0.6221479)


class TestDeath:
    """
    Tests the different ways for an animal to die.
    """
    omega = 0.4
    alpha = 0.05

    @pytest.fixture(autouse=True)
    def create_herbivore(self):
        """
        Creates some animals to use for the tests.
        """
        self.herbivore = Herbivore(weight=5)
        self.carnivore = Carnivore(weight=5)
        self.probability_death = self.omega * (1 - self.herbivore.fitness)

    def test_death_zero_fitness(self):
        """
        Tests that an animal with zero fitness dies.
        """
        self.herbivore.fitness = 0
        assert bool(self.herbivore.probability_death()) is True

    def test_death_max_fitness(self):
        """
        Tests that the probability_death method does not make an animal with fitness 1 die
        """
        unfit_herbivore = Herbivore()
        unfit_herbivore.fitness = 1
        assert unfit_herbivore.probability_death() is False

    def test_death_probability(self):
        """
        Tests probability_death. stats.binom_test makes sure the actual number of
        dead herbivores coincides with the probability.
        """
        herb_pop = [Herbivore(weight=5) for _ in range(1000)]
        deaths = 0
        for herb in herb_pop:
            if herb.probability_death() is True:
                deaths += 1

        prob = stats.binom_test(deaths, len(herb_pop), self.probability_death)
        assert prob > self.alpha


class TestBirth:
    """
    Tests that makes sure new animals are being created properly
    """
    default_herbivore_parameters = {
        'w_birth': 8.0, 'sigma_birth': 1.5, 'beta': 0.9, 'eta': 0.05,
        'a_half': 40.0, 'phi_age': 0.2, 'w_half': 10.0, 'phi_weight': 0.1,
        'mu': 0.25, 'lambda': 1.0, 'gamma': 0.2, 'zeta': 3.5, 'xi': 1.2,
        'omega': 0.4, 'F': 10.0
    }

    @pytest.fixture(autouse=True)
    def create_herbivore(self):
        """
        Creates some animals to use for the tests.
        """
        self.herbivore = Herbivore()
        self.large_herbivore = Herbivore(weight=50)
        self.small_herbivore = Herbivore(weight=1)

    @pytest.fixture(autouse=True)
    def reset_parameters(self):
        """
        Resets the parameters used in the tests
        """
        BioSim.set_animal_parameters('Herbivore', self.default_herbivore_parameters)

    def test_probability_pregnancy(self):
        """
        Tests that the probability to have offspring is correct based on the
        number of animals in the cell.
        """
        assert self.herbivore.probability_pregnancy(1) == 0
        assert self.herbivore.probability_pregnancy(100) == 1

    def test_weight_check_pregnancy(self):
        """
        Tests that the animal has enough weight in order to give birth
        """
        assert self.large_herbivore.weight >= self.large_herbivore.weight_check_pregnancy()
        assert self.small_herbivore.weight < self.small_herbivore.weight_check_pregnancy()

    def test_birth_update_weight(self):
        """
        Tests that the weight of the animal updates after giving birth
        """
        initial_weight = self.large_herbivore.weight
        self.large_herbivore.birth_update_weight(self.herbivore)
        assert self.large_herbivore.weight == initial_weight - (
                self.large_herbivore.parameters['xi'] * self.herbivore.weight)

    def test_birth_herbivore(self):
        """
        Tests that herbivore population list gets updated after an herbivore gives birth.
        """
        herbivore = Herbivore(weight=50)
        assert herbivore.weight >= herbivore.weight_check_pregnancy()
        new_born = herbivore.birth(n=100)
        assert isinstance(new_born, Herbivore)

    def test_birth_carnivore(self):
        """
        Tests that herbivore population list gets updated after an herbivore gives birth.
        """
        carnivore = Carnivore(weight=50)
        assert carnivore.weight >= carnivore.weight_check_pregnancy()
        new_born = carnivore.birth(n=100)
        assert isinstance(new_born, Carnivore)

    def test_birth_too_low_weight(self):
        """
        Tests that a herbivore with less weight than required is not able to have
        offspring
        """
        assert self.small_herbivore.birth(n=10) is None

    def test_no_birth(self):
        """
        Tests that a herbivore that passes the weight check, is still not able
        to have offspring. To test this, the gamma parameter is set to a low number so
        that it fails the probability check
        """
        self.large_herbivore.parameters['gamma'] = 0.0001
        assert self.large_herbivore.birth(n=10) is None

    def test_params(self):
        """
        Tests that parameters are reset.
        """
        assert self.herbivore.parameters['gamma'] == 0.2


class TestMigrate:
    """
    Tests that migration functions properly
    """

    @pytest.fixture(autouse=True)
    def create_animals(self):
        """
        Creates some animals for the tests
        """
        self.herbivore = Herbivore()
        self.carnivore = Carnivore()
        self.nearby_cells = [Highland(), Lowland(), Desert(), Highland()]
        self.nearby_cells_water = [Water(), Water(), Water(), Water()]

    def test_migrate_successful(self):
        """
        Tests that an animal can migrate. This test makes sure the chosen destination exist,
        and that the has_moved attribute is updated.
        """
        self.herbivore.fitness = 10
        assert self.herbivore.has_moved is False
        destination = self.herbivore.migrate(self.nearby_cells)
        assert self.herbivore.has_moved is True
        assert destination is not None

    def test_migrate_too_low_fitness(self):
        """
        Tests that no migration can happen when an animal does not pass the fitness check.
        """
        self.herbivore.fitness = 0.001
        destination = self.herbivore.migrate(self.nearby_cells)
        assert self.herbivore.has_moved is False
        assert destination is None

    def test_migrate_unsuccessful(self):
        """
        Tests that an animal can not migrate when it chooses a water cell. The has_moved
        attribute is to stay False, and the destination should be None.
        """
        self.herbivore.fitness = 10
        assert self.herbivore.has_moved is False
        destination = self.herbivore.migrate(self.nearby_cells_water)
        assert self.herbivore.has_moved is False
        assert destination is None


class TestKillFeeding:
    """
    Tests for the kill and feeding mechanics
    """

    default_carnivore_parameters = {
        'w_birth': 6.0, 'sigma_birth': 1.0, 'beta': 0.75, 'eta': 0.125,
        'a_half': 60.0, 'phi_age': 0.4, 'w_half': 4.0, 'phi_weight': 0.4,
        'mu': 0.4, 'lambda': 1.0, 'gamma': 0.8, 'zeta': 3.5, 'xi': 1.1,
        'omega': 0.9, 'F': 50.0, 'DeltaPhiMax': 10.0
    }

    @pytest.fixture(autouse=True)
    def create_animals(self):
        """
        Creates some animals to test
        """
        self.herbivore = Herbivore(weight=5)
        self.carnivore = Carnivore(weight=5)

        self.nearby_herbivores = [Herbivore()]

        self.weak_herbivore = Herbivore(weight=10)
        self.weak_herbivore.fitness = 0.01

        self.weak_carnivore = Carnivore(weight=10)
        self.weak_carnivore.fitness = 0.01

        self.nearby_weak_herbivores = [self.weak_herbivore for _ in range(10)]
        self.few_nearby_weak_herbivores = [self.weak_herbivore for _ in range(2)]

        self.cell_max_fodder_amount = 300
        self.cell_min_fodder_amount = 1

    @pytest.fixture(autouse=True)
    def reset_parameters(self):
        """
        Resets the parameters after the tests
        """
        yield
        BioSim.set_animal_parameters('Carnivore', self.default_carnivore_parameters)

    def test_herbivore_feeding_with_max_fodder(self):
        """
        Tests that an herbivore is eating 'F' amount when feeding in a cell with max fodder.
        Also makes sure the weight is updated accordingly.
        """
        consumed = self.herbivore.feed(self.cell_max_fodder_amount)
        assert consumed == 10
        assert self.herbivore.weight == 14

    def test_herbivore_feeding_with_min_fodder(self):
        """
        Tests that an herbivore is eating what's available when feeding in a cell with a
        limited amount of fodder. Also makes sure weight is updated accordingly
        """
        consumed = self.herbivore.feed(self.cell_min_fodder_amount)
        assert consumed == 1
        assert self.herbivore.weight == 5.9

    def test_fitness_higher_than_pray(self):
        """
        Tests that a carnivore passes the fitness_greater_than_pray check when attacking
        a weak herbivore
        """
        assert self.carnivore.fitness_higher_than_prey_fitness(self.weak_herbivore)

    def test_kill_probability(self):
        """
        Tests that the kill probability returns expected number
        """
        assert self.carnivore.kill_probability(self.weak_herbivore) == approx(0.05886876)

    def test_single_kill(self):
        """
        Tests that when a very fit carnivore attempts to kill an herbivore, it is added
        to the killed_herbivores list. Also makes sure that the weight and fitness of
        the carnivore is updated. Fitness should now be a "normaL" value between 0 and 1.
        """
        self.carnivore.fitness = 100
        killed_herbivores = self.carnivore.kill(self.nearby_herbivores)

        assert isinstance(killed_herbivores[0], Herbivore)
        assert len(killed_herbivores) == 1
        assert self.carnivore.weight > 5
        assert 0 < self.carnivore.fitness < 1
        assert isinstance(killed_herbivores, list)

    def test_no_kill(self):
        """
        Tests that when a weak carnivore attempts to kill an herbivore, it is not added
        to the killed_herbivores list. Also checks that it's weight remain the same.
        """
        killed_herbivore = self.weak_carnivore.kill(self.nearby_herbivores)

        assert len(killed_herbivore) == 0
        assert self.weak_carnivore.weight == 10

    def test_carnivore_eating_f_limit(self):
        """
        Tests that a carnivore can kill no more than it's 'F' amount.
        """
        self.carnivore.parameters['DeltaPhiMax'] = 0.0001
        killed_herbivores = self.carnivore.kill(self.nearby_weak_herbivores)
        assert len(killed_herbivores) == 5

    def test_carnivore_eating_herb_limit(self):
        """
        Tests that a carnivore stops killing when there is no more herbivores left to kill.
        """
        self.carnivore.parameters['DeltaPhiMax'] = 0.0001
        killed_herbivores = self.carnivore.kill(self.few_nearby_weak_herbivores)
        assert len(killed_herbivores) == 2
        assert self.carnivore.weight > 5

    def test_params(self):
        """
        Tests that parameters are reset.
        """
        assert self.carnivore.parameters['DeltaPhiMax'] == 10.0
