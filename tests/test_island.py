#!python
# -*- coding: utf-8 -*-


"""
Tests for herbivores.
"""

__author__ = "Vetle Reinholt, Kim Næss Kynningsrud"
__email__ = "vetle.aasen.reinholt@nmbu.no, kim.nass.kynningsrud@nmbu.no"

import pytest
import textwrap
from biosim.simulation import BioSim
from biosim.island import Island


class TestIsland:
    """
    Tests for island class
    """

    default_lowland_params = {
        'f_max': 800
    }
    default_highland_params = {
        'f_max': 300
    }

    @pytest.fixture(autouse=True)
    def reset_params(self):
        """
        Resets all parameters to default values
        """
        BioSim.set_landscape_parameters('L', self.default_lowland_params)
        BioSim.set_landscape_parameters('H', self.default_highland_params)

    @pytest.fixture(autouse=True)
    def island_setup(self):
        """
        Making a test island to run the tests on
        """
        test_map = """\
                        LLLLLLL
                        WDDLLLW
                        WWDHHLW
                        WWWWWWW"""
        self.test_map = textwrap.dedent(test_map)

        test_population = [
            {
                "loc": (0, 0),
                "pop": [
                    {"species": "Herbivore", "age": 1, "weight": 10},
                    {"species": "Carnivore", "age": 1, "weight": 10},
                ],
            },
            {
                "loc": (1, 3),
                "pop": [
                    {"species": "Carnivore", "age": 15, "weight": 50},
                    {"species": "Herbivore", "age": 25, "weight": 20},
                    {"species": "Herbivore", "age": 2, "weight": 10}
                ],
            },
        ]

        self.test_population = test_population
        self.island = Island(self.test_map, self.test_population)

    def test_nearby_cells(self):
        nearby_cells = self.island.nearby_cells(row=1, col=1)
        assert isinstance(nearby_cells, list)

    def test_adding_population(self):
        self.island.adding_population(self.test_population)

        pop_0 = self.island.init_map(self.test_population)[0][0].carnivores
        assert isinstance(pop_0, list)
