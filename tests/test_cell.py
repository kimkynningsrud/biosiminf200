#!python
# -*- coding: utf-8 -*-

__author__ = "Vetle Reinholt, Kim Næss Kynningsrud"
__email__ = "vetle.aasen.reinholt@nmbu.no, kim.nass.kynningsrud@nmbu.no"

import pytest
from pytest import approx
from pytest_mock import mocker

from biosim.cell import Cell, Lowland, Highland, Water, Desert
from biosim.animals import Animals, Herbivore, Carnivore
from biosim.simulation import BioSim


class TestCell:
    """
    Tests for the methods on the cell level
    """
    lowland_param = {'f_max': 800}
    highland_param = {'f_max': 300}

    default_herbivore_parameters = {
        'w_birth': 8.0, 'sigma_birth': 1.5, 'beta': 0.9, 'eta': 0.05,
        'a_half': 40.0, 'phi_age': 0.2, 'w_half': 10.0, 'phi_weight': 0.1,
        'mu': 0.25, 'lambda': 1.0, 'gamma': 0.2, 'zeta': 3.5, 'xi': 1.2,
        'omega': 0.4, 'F': 10.0
    }
    default_highland_parameter = {
        'f_max': 300
    }
    default_lowland_parameter = {
        'f_max': 800
    }

    @pytest.fixture(autouse=True)
    def reset_parameters(self):
        """
        Resets the parameters after the tests
        """
        BioSim.set_animal_parameters('Carnivore', self.default_herbivore_parameters)
        BioSim.set_landscape_parameters('H', self.default_highland_parameter)
        BioSim.set_landscape_parameters('L', self.default_lowland_parameter)

    @pytest.fixture(autouse=True)
    def setup_cells(self):
        """
        Sets up necessary variables for testing cell methods
        """
        self.single_herb = Herbivore()
        self.single_carn = Carnivore()
        self.lowland_cell = Lowland()
        self.highland_cell = Highland()
        self.desert_cell = Desert()
        self.water_cell = Water()
        self.nearby_cells = [Highland(), Highland(), Highland(), Highland()]
        self.test_pop = [Herbivore(age=3, weight=10),
                         Carnivore(age=5, weight=20),
                         Herbivore(age=8, weight=16)]

        self.herb_pop = [Herbivore(age=3, weight=10),
                         Herbivore(age=5, weight=20),
                         Herbivore(age=8, weight=16)]

        self.carn_pop = [Carnivore(age=1, weight=5),
                         Carnivore(age=5, weight=10),
                         Carnivore(age=8, weight=16)]

        self.empty_pop = []

        self.animals_to_add = [
                    {"species": "Herbivore", "age": 5, "weight": 20},
                    {"species": "Carnivore", "age": 10, "weight": 5},
                    {"species": "Herbivore", "age": 25, "weight": 20},
                    {"species": "Carnivore", "age": 15, "weight": 50}]


    def test_cell_type(self):
        """
        Tests that the cell type is set up the correct way.
        """
        assert self.highland_cell.parameters['f_max'] == 300
        assert self.lowland_cell.parameters['f_max'] == 800
        assert isinstance(self.highland_cell, Highland)
        assert isinstance(self.lowland_cell, Lowland)
        assert isinstance(self.desert_cell, Desert)
        assert isinstance(self.water_cell, Water)

    def test_get_num_herbivores(self):
        """
        Tests the get_num_herbivores method. Sets a herbivore population in a cell and checks if the
        method returns the correct amount of herbivores.
        """
        self.highland_cell.herbivores = self.herb_pop
        assert self.highland_cell.get_num_herbivores() == 3

    def test_get_num_carnivores(self):
        """
        Tests the get_num_carnivores method. Sets a carnivore population in a cell and checks if the
        method returns the correct amount of carnivores.
        """
        self.lowland_cell.carnivores = self.carn_pop
        assert self.lowland_cell.get_num_carnivores() == 3

    def test_add_animals(self):
        """
        Tests that add_animals works as intended. We add some animals using the function
        and check that the animal lists are updated
        """
        self.desert_cell.herbivores = self.herb_pop
        self.desert_cell.carnivores = self.carn_pop
        self.desert_cell.add_animals(self.animals_to_add)
        assert len(self.desert_cell.herbivores) == 5
        assert len(self.desert_cell.carnivores) == 5


    def test_sort_herb(self):
        """
        Tests the sort_herb method by making sure the first herbivore in the herbivore population
        has less fitness than the second.
        """
        sorted_by_fitness = self.desert_cell.sort_herb(self.test_pop)
        assert sorted_by_fitness[0].fitness < sorted_by_fitness[1].fitness

    def test_sort_carn(self):
        """
        Tests the sort_carn method by making sure the first carnivore in the carnivore population
        has more fitness than the second. The sort_carn method reverses the order of the list.
        """
        sorted_by_fitness = self.desert_cell.sort_carn(self.test_pop)
        assert sorted_by_fitness[0].fitness > sorted_by_fitness[1].fitness

    def test_feeding_fodder_update(self):
        """
        Tests that the feeding method successfully removes the fodder eaten in a cell.
        """
        self.highland_cell.fodder = 300
        self.highland_cell.herbivores = self.herb_pop
        self.highland_cell.feeding()
        assert self.highland_cell.fodder == 300 - 10 * 3

    def test_feeding_population_update(self):
        """
        Tests that the list of herbivores updates after one has been eaten by a carnivore.
        """
        self.single_carn.fitness = 10
        self.lowland_cell.herbivores = self.herb_pop
        self.lowland_cell.carnivores = [self.single_carn]
        self.lowland_cell.feeding()
        assert self.lowland_cell.get_num_herbivores() == 2

    def test_procreation_successful(self):
        """
        Tests that procreation increases the number of herbivores. Here we set the
        parameters used in the weight_check_pregnancy and probability_pregnancy to large
        numbers to ensure a birth.
        """
        Herbivore().parameters['gamma'] = 100
        Herbivore().parameters['zeta'] = 0
        self.highland_cell.herbivores = self.herb_pop
        ini_cell_pop = len(self.highland_cell.herbivores)
        self.highland_cell.procreation()
        assert len(self.highland_cell.herbivores) == ini_cell_pop + 3

    def test_procreation_unsuccessful(self):
        """
        Tests that no procreation happens when there are no animals left. Parameters
        are set to a large number to ensure birth.
        """
        Herbivore().parameters['mu'] = 100
        self.highland_cell.herbivores = self.empty_pop
        self.highland_cell.procreation()
        assert len(self.highland_cell.herbivores) == 0

    def test_migration_successful_removed_from_cell(self):
        """
        Tests that the list of animals in the current cell gets updated
        when a migration happen. Parameters are set to a large number to
        ensure birth.
        """
        Herbivore().parameters['mu'] = 100
        self.highland_cell.herbivores = self.herb_pop
        self.highland_cell.migration(self.nearby_cells)
        assert len(self.highland_cell.herbivores) < 3

    def test_migration_unsuccessful_no_animals(self):
        """
        Tests that no migrations can happen when there are no animals left in the
        cell.
        """
        Herbivore().parameters['mu'] = 100
        self.highland_cell.herbivores = self.empty_pop
        self.highland_cell.migration(self.nearby_cells)
        assert len(self.highland_cell.herbivores) == 0

    def test_migration_unsuccessful_has_moved(self):
        """
        Tests that no migrations can happen when the animals has_moved tag is set to
        True. This makes sure no animal can move twice.
        """
        Herbivore().parameters['mu'] = 100
        for herb in self.herb_pop:
            herb.has_moved = True

        self.lowland_cell.herbivores = self.herb_pop
        self.lowland_cell.migration(self.nearby_cells)
        assert len(self.lowland_cell.herbivores) == 3

    def test_aging_weightloss(self):
        """
        Tests that the aging_weightloss method runs successfully. Tests that the age increases
        by one after a year, and also that the weight decreases according to the given formula.
        """
        self.highland_cell.herbivores = self.herb_pop
        self.highland_cell.aging_weightloss()
        assert self.highland_cell.herbivores[0].age == 4
        assert self.highland_cell.herbivores[0].weight == 9.5

    def test_death(self):
        """
        Tests that the death method runs successfully. Checks that an herbivore
        is removed from the herbivore population list after it dies by zero fitness.
        """
        self.herb_pop[0].fitness = 0
        self.highland_cell.herbivores = self.herb_pop
        assert len(self.highland_cell.herbivores) == 3
        self.highland_cell.death()
        assert len(self.highland_cell.herbivores) < 3

    def test_fodder_gen(self):
        """
        Tests that the fodder generation works for all cell types, by setting the fodder
        amount to zero, and then generating more.
        """

        cell_type = [Lowland(), Highland(), Desert()]

        for cell in cell_type:
            cell.fodder = 0
            cell.fodder_gen()

        assert self.highland_cell.fodder == 300
        assert self.lowland_cell.fodder == 800
        assert self.desert_cell.fodder == 0

    def test_is_accessible(self):
        """
        Tests that the different cell types have the right configuration to avoid
        having animals migrating to water.
        """
        assert self.lowland_cell.is_accessible == True
        assert self.highland_cell.is_accessible == True
        assert self.desert_cell.is_accessible == True
        assert self.water_cell.is_accessible == False
