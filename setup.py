"""
To create a package, run

python setup.py sdist

in the directory containing this file.

To create a zip archive in addition to a tar.gz archive, run

python setup.py sdist --formats=gztar,zip

The package will be placed in directory dist.

To install from the package, unpack it, move into the unpacked directory and
run

python setup.py install          # default location
python setup.py install --user   # per-user default location
python setup.py install --prefix=/Users/plesser/tmp/test  # to given dir
"""

from setuptools import setup

setup(
      name='BioSim',
      version='1.0',
      description='Simulation of ecosystem',
      author='Vetle Reinholt, Kim Næss Kynningsrud, NMBU',
      author_email='vetle.aasen.reinholt@nmbu.no, kim.nass.kynningsrud@nmbu.no',
      requires=['numpy', 'matplotlib'],
      packages=['biosim'],
)
