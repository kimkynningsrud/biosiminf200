Project description
===================
Overview
--------
Rossumøya is a small island in the middle of the vast ocean that belongs to the island
nation of Py-landia. The ecosystem on Rossumøya is relatively undisturbed, but Pylandia’s
Environmental Protection Agency wants to study the stability of the ecosys-tem.
The long term goal is to preserve Rossumøya as a nature park for future generations.
The ecosystem on Rossumøya is characterized by several different landscape types, lowland,
highlandand, desert. The fauna includes only two species, one species of herbivores (plant eaters),
and one of carnivores (predators).You shall investigate whetherboth species can survive in the long term.


Geography
---------
Rossumøya is divided into squares (or cells), where each square is of one of the following
five types:
- water
- desert
- highland
- lowland

As an island, Rossumøya is surrounded by water. Cells on the outer edges of the map of Rossumøya are therefore
always of type “Water”. An animal that is in the middle cell can move to one of the four neighboring
cells but not to cells diagonally displaced. Animals can only move from the square they are in to one
of the four nearest neighbor squares, see Fig. 2. No animal may stay in water.

Water: Water cannot be entered by animals. Cells of type will be completely passive in the simulation.
Water may be sea surrounding the island or lakes within the island

Desert: Animals may stay in the desert, but there is no fodder available to herbivores there.
Carnivores can prey on herbivores in the desert

Highland: Herbivores will find fodder in the highland. Each year, a fixed amount of fodder is
available. Carnivores can prey on herbivores in the highland

Lowland: The same rules apply as to the highland, but usually more fodder is available in the lowland than
the high-land


Fauna
-----
Herbivores and carnivores have certain characteristics in common, but feed in different ways.
The similarities are as follows:

Age
 - At birth, each animal has age a=0 . Age increases by one year for each year that passes.

Weight
 - Animals are born with weight w∼N(wbirth,σbirth), i.e., the birth weight is drawn from a
    Gaussian distribution with mean wbirth and standard deviation σbirth. When an animal eats an amount F of
    fodder, its weight increases by βF. Every year, the weight of the animal decreases by ηw

Fitness
 - The overall condition of the animal is described by its fitness, which is calculated based on age
    and weight using the following formula

    Φ={0w≤0q+(a,a12,φage)×q−(w,w12,φweight) where q±(x,x12,φ) = 1/1+e±φ(x−x1/2). Note that 0≤Φ≤1.

Migration
 - Animals migrate depending on their own fitness. Animals can only move to the four immediately adjacent
    cells. Animals cannot move to water cells. An animal moves with probability μΦ. If an animal moves, the
    destination cell is chosen at random between the four nearest neighbor cells, with
    equal probability for each direction. If the selected destination cell is Water, the animal does not
    move. An animal can walk only once per year

Birth
 - Animals can mate if there are at least two animals of the same species in a cell.
    * For each animal in a cell, the probability to give birth to an offspring in a year is
        min(1,γ×Φ×(N−1)), where N is the number of animals of the same species in the cell at the
        start of the breeding season.

    * The probability is therefore 0 if there is only one individual of the species in the cell.

    * The probability of birth is also 0 if the weight is w<ζ(wbirth+σbirth).

    * Gender plays no role in mating.

    * Each animal can give birth to at most one off-spring per year.

    * At birth, the mother animal loses ξ times the actual birt hweight of the baby.

    * If the mother would lose more than her own weight, then no baby is born and the weight of the mother
        remains unchanged

Death
 - An animal dies
    * with certainty if its weight is w=0;
    * with probability ω(1−Φ) otherwise

Animal types
------------
Herbivores
 - Herbivores find fodder exclusively in the low- and highland. Animals residing in a cell eat
    in random order. Each animal tries every year to eat an amount F of fodder, but how much feed the animal
    obtain depends on fodder available in the cell. Given that the animal eats an amount ̃F of fodder, its weight
    increases by β̃F

Carnivores
 - Carnivores can prey on herbivores everywhere, but do not prey on each other. Carnivores try to kill
    herbivores in the order of fitness, i.e., the carnivore with the highest fitness eats first. Carnivores
    try to kill one herbivore at a time, beginning with the herbivore with the lowest fitness.
    Carnivores will kill a herbivore with probability (Φcarn−Φherb)/∆Φmax

Annual Cycle
------------
Nature on Rossumøya follows a fixed annual cycle. The components of annual cycle are:

Feeding
 - Animals eat: first herbivores, then carnivores, Growth of fodder in lowland and highland occurs at the
    very beginning of the year, i.e., immediately before herbivores eat

Procreation
 - Animals give birth. When calculating the probability of birth, the number of animals N at the start of the
    breeding season is used, i.e., newborn animals do not count.

Migration
 - Animals migrate to neighboring cells subject to the condition that each animal can migrate at most
    once per year

Aging
 - Each animal becomes one year older

Loss of weight
 - All animals lose weight.

Death
 - For each animal, it is determined whether the animal dies or not.
