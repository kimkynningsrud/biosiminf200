.. BioSim documentation master file, created by
   sphinx-quickstart on Tue Jan 19 11:59:12 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BioSim's documentation!
==================================

BioSim is a simulation of the ecosystem of Rossumøya. It attempts to mimic the wildlife of an island through
different methods such as: feeding, killing, migration, birth etc. BioSim also contains a visualization tool to
illustrate population count as well as a heat map for animal position.

The different files, as well as documentation, can be found here:


.. toctree::
   :maxdepth: 2

   project_description
   animals
   cell
   island
   graphics




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
