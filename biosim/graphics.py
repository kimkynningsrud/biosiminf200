#!python
# -*- coding: utf-8 -*-

__author__ = "Vetle Reinholt, Kim Næss Kynningsrud"
__email__ = "vetle.aasen.reinholt@nmbu.no, kim.nass.kynningsrud@nmbu.no"

import matplotlib.pyplot as plt
import numpy as np
import subprocess

# Update these variables to point to your ffmpeg and convert binaries
# If you installed ffmpeg using conda or installed both software in
# standard ways on your computer, no changes should be required.
_FFMPEG_BINARY = 'ffmpeg'
_MAGICK_BINARY = 'magick'

# update this to the directory and file-name beginning
# for the graphics files
_DEFAULT_IMG_FORMAT = 'png'
_DEFAULT_MOVIE_FORMAT = 'mp4'  # alternatives: mp4, gif


class Graphics:
    """
    Provides graphics for BioSim
    """

    def __init__(self, img_base=None,
                 img_fmt=None,
                 ymax_animals=None,
                 cmax_animals=None,
                 hist_specs=None):
        """
        :param img_base: directory for image files; no images if None
        :type img_base: str
        :param img_fmt: image file format suffix
        :type img_fmt: str
        :param ymax_animals: max value for y-axis on animal number graph
        :type ymax_animals: int
        :param cmax_animals: specifications for max value of heat map color bar
        :type cmax_animals: dict
        :param hist_specs: specifications for histograms
        :type hist_specs: dict
        """

        self._img_base = img_base

        self._img_fmt = img_fmt if img_fmt is not None else _DEFAULT_IMG_FORMAT

        self._img_ctr = 0
        self._img_year = 1

        self._fig = None
        self._herb_heat_map_ax = None
        self._carn_heat_map_ax = None
        self._herb_img_axis = None
        self._carn_img_axis = None
        self._animal_ax = None

        self._fitness_ax = None
        self._age_ax = None
        self._weight_ax = None

        self.herb_line = None
        self.carn_line = None

        self._timer = None
        self._txt = None
        self.template = None

        self._static_map = None

        self.ymax_animals = ymax_animals
        self.cmax_animals = cmax_animals
        self.hist_specs = hist_specs

    def update(self, year, herb_matrix, carn_matrix, num_animals):
        """
        Updates the graphics
        """
        num_herbs, num_carns = num_animals
        self._update_heat_map(herb_matrix, carn_matrix)
        self._update_herb_graph(year, num_herbs)
        self._update_carn_graph(year, num_carns)
        self._update_timer(year)
        self._fig.canvas.flush_events()
        plt.pause(1e-6)

        self._save_graphics(year)

    def make_movie(self, movie_fmt=None):
        """
        Makes a movie from the images of the graphics
        """

        if self._img_base is None:
            raise RuntimeError('No filename defined.')

        if movie_fmt is None:
            movie_fmt = _DEFAULT_MOVIE_FORMAT

        if movie_fmt == 'mp4':
            try:
                # Parameters chosen according to http://trac.ffmpeg.org/wiki/Encode/H.264,
                # section "Compatibility"
                subprocess.check_call([_FFMPEG_BINARY,
                                       '-i', '{}_%05d.png'.format(self._img_base),
                                       '-y',
                                       '-profile:v', 'baseline',
                                       '-level', '3.0',
                                       '-pix_fmt', 'yuv420p',
                                       '{}.{}'.format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: ffmpeg failed with: {}'.format(err))
        elif movie_fmt == 'gif':
            try:
                subprocess.check_call([_MAGICK_BINARY,
                                       '-delay', '1',
                                       '-loop', '0',
                                       '{}_*.png'.format(self._img_base),
                                       '{}.{}'.format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: convert failed with: {}'.format(err))
        else:
            raise ValueError('Unknown movie format: ' + movie_fmt)

    def setup(self, final_year, img_year):
        """
        Prepares graphics.
        """
        self._img_year = img_year

        if self._fig is None:
            self._fig = plt.figure()

        if self.ymax_animals is None:
            self.ymax_animals = 20000

        if self.cmax_animals is None:
            self.cmax_animals = {'Herbivore': 200, 'Carnivore': 50}

        if self._timer is None:
            self._timer = self._fig.add_axes([0.3, 0.8, 0.2, 0.2])
            self._timer.axis('off')

            self.template = 'Year: {:5d}'
            self._txt = self._timer.text(0.5, 0.5, self.template.format(0),
                                         horizontalalignment='center',
                                         verticalalignment='center',
                                         transform=self._timer.transAxes,
                                         weight='bold',
                                         fontsize=25)

        if self._herb_heat_map_ax is None:
            self._herb_heat_map_ax = self._fig.add_axes([0.1, 0.5, 0.3, 0.3])
            self._herb_img_axis = None
            plt.title('Herbivore distribution')

        if self._carn_heat_map_ax is None:
            self._carn_heat_map_ax = self._fig.add_axes([0.1, 0.1, 0.3, 0.3])
            self._carn_img_axis = None
            plt.title('Carnivore distribution')

        if self._animal_ax is None:
            self._animal_ax = self._fig.add_subplot(2, 2, 2)
            self._animal_ax.set_ylim(0, self.ymax_animals)
            plt.title('Animal count', fontsize='20')

        self._animal_ax.set_xlim(0, final_year + 1)

        if self.herb_line or self.carn_line is None:
            herb_plot = self._animal_ax.plot(
                np.arange(0, final_year + 1),
                np.full(final_year + 1, np.nan),
                'b-',
                label='Herbivore'
            )
            carn_plot = self._animal_ax.plot(
                np.arange(0, final_year + 1),
                np.full(final_year + 1, np.nan),
                'r-',
                label='Carnivore'
            )
            self.herb_line = herb_plot[0]
            self.carn_line = carn_plot[0]

            plt.legend(loc=4, prop={'size': 10})
        else:
            xdata_herb, ydata_herb = self.herb_line.get_data()
            x_new_herb = np.arange(xdata_herb[-1] + 1, final_year + 1)
            if len(x_new_herb) > 0:
                y_new_herb = np.full(x_new_herb.shape, np.nan)
                self.herb_line.set_data(np.hstack((xdata_herb, x_new_herb)),
                                        np.hstack((ydata_herb, y_new_herb)))

    def make_map(self, island_map):
        """
        Creates a static map of the island.
        """
        rgb_values = {'W': (0.0, 0.0, 1.0),  # blue
                      'L': (0.0, 0.6, 0.0),  # dark green
                      'H': (0.5, 1.0, 0.5),  # light green
                      'D': (1.0, 1.0, 0.5)}  # light yellow

        static_map = [[rgb_values[column] for column in row]
                      for row in island_map.splitlines()]
        self._static_map = self._fig.add_axes([0.38, 0.08, 0.7, 0.4])

        self._static_map.imshow(static_map)

        self._static_map.set_xticks(range(len(static_map[0])))
        self._static_map.set_xticklabels(range(1, 1 + len(static_map[0])))
        self._static_map.set_yticks(range(len(static_map)))
        self._static_map.set_yticklabels(range(1, 1 + len(static_map)))

        axlg = self._fig.add_axes([0.90, 0.1, 0.1, 0.8])
        axlg.axis('off')
        for ix, name in enumerate(('Water', 'Lowland',
                                   'Highland', 'Desert')):
            axlg.add_patch(plt.Rectangle((0., ix * 0.1), 0.3, 0.05,
                                         edgecolor='none',
                                         facecolor=rgb_values[name[0]]))
            axlg.text(0.35, ix * 0.1, name, transform=axlg.transAxes)

    def _update_heat_map(self, herb_matrix, carn_matrix):
        """
        Update the heat maps of the island.
        """
        if self._herb_img_axis is not None:
            self._herb_img_axis.set_data(herb_matrix)
        else:
            self._herb_img_axis = self._herb_heat_map_ax.imshow(herb_matrix,
                                                                interpolation='nearest',
                                                                vmax=self.cmax_animals['Herbivore'])
            plt.colorbar(self._herb_img_axis, ax=self._herb_heat_map_ax,
                         orientation='vertical')

        if self._carn_img_axis is not None:
            self._carn_img_axis.set_data(carn_matrix)
        else:
            self._carn_img_axis = self._carn_heat_map_ax.imshow(carn_matrix,
                                                                interpolation='nearest',
                                                                vmax=self.cmax_animals['Carnivore'])
            plt.colorbar(self._carn_img_axis, ax=self._carn_heat_map_ax,
                         orientation='vertical')

    def _update_herb_graph(self, year, num_herbs):
        """
        Updates the herbivore graph
        """
        ydata_h = self.herb_line.get_ydata()
        ydata_h[year] = num_herbs
        self.herb_line.set_ydata(ydata_h)

    def _update_carn_graph(self, year, num_carns):
        """
        Updates the carnivore graph
        """
        ydata_c = self.carn_line.get_ydata()
        ydata_c[year] = num_carns
        self.carn_line.set_ydata(ydata_c)

    def _update_timer(self, year):
        """
        Updates the timer
        """
        self._txt.set_text(self.template.format(year))

    def _save_graphics(self, year):
        """
        Saves the graphics to a file if the file name is given.
        """
        if self._img_base is None or year % self._img_year != 0:
            return
        plt.savefig('{base}_{num:05d}.{type}'.format(base=self._img_base,
                                                     num=self._img_ctr,
                                                     type=self._img_fmt))
        self._img_ctr += 1
