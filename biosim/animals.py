#!python
# -*- coding: utf-8 -*-

__author__ = "Vetle Reinholt, Kim Næss Kynningsrud"
__email__ = "vetle.aasen.reinholt@nmbu.no, kim.nass.kynningsrud@nmbu.no"

import math
import random


class Animals:
    """
    Class for animals
    """
    parameters = {}

    def __init__(self, age=0, weight=None):
        """
        Class constructor for Herbivore.
        Creates an Herbivore with age, weight and fitness as instance attributes.

        Parameters
        ----------
        age: int
            Age of the animal
        weight: float
            Weight of the animal
        """
        self.has_moved = False
        self.age = age
        if weight is None:
            self.weight = self.calculate_weight()
        else:
            self.weight = weight
        self.fitness = self.calculate_fitness()

    def aging(self):
        """
        Adds one year of age after each year simulated,
        and updates fitness as it is dependant on age.
        """
        self.age += 1
        self.update_fitness()

    def calculate_weight(self):
        """
        Calculates the weight of the animal.
        """
        return random.gauss(self.parameters['w_birth'], self.parameters['sigma_birth'])

    def weight_decrease(self):
        """
        Decreases weight each year and updates fitness.
        """
        self.weight -= self.weight * self.parameters['eta']
        self.update_fitness()

    def weight_increase(self, consumed):
        """
        Increases weight after consuming food.

        Parameters
        ----------
        consumed: float
            Amount of food consumed

        Returns
        -------
        Weight increase: float
            The weight the animal is increased by
        """
        return consumed * self.parameters['beta']

    def calculate_fitness(self):
        """
        Calculates the fitness.

        Returns
        -------
        Fitness expression: float
            The new fitness of the animal
        """
        if self.weight <= 0:
            return 0
        else:
            return (1 / (1 + math.exp(self.parameters['phi_age'] * (
                    self.age - self.parameters['a_half'])))) \
                   * (1 / (1 + math.exp(-(self.parameters['phi_weight'] *
                                          (self.weight - self.parameters['w_half'])))))

    def update_fitness(self):
        """
        Updates the fitness.
        """
        self.fitness = self.calculate_fitness()

    def probability_death(self):
        """
        Makes the animal die if fitness reaches zero. It also has a probability to die each
        year dependant on its fitness.

        Returns
        -------
        Probability: bool
            Returns true or false to ascertain whether the animal
            dies or not at the end of the year.
        """
        if self.fitness == 0:
            return True
        return random.random() < self.parameters['omega'] * (1 - self.fitness)

    def probability_pregnancy(self, n):
        """
        Determines the probability of pregnancy. It also makes sure that at least two
        animals are required.

        Returns
        -------
        Probability: float
            Returns the probability of pregnancy as a float.
        """
        if n < 2:
            return 0
        else:
            return min(1, self.parameters['gamma'] * self.fitness * (n - 1))

    def weight_check_pregnancy(self):
        """
        Checks if the animal has enough weight in order to give birth.
        """
        return self.parameters['zeta'] * (
                self.parameters['w_birth'] + self.parameters['sigma_birth'])

    def birth_update_weight(self, new_born):
        """
        Updates the weight of the animal after giving birth.
        """
        self.weight -= self.parameters['xi'] * new_born.weight

    def birth(self, n):
        """
        Animals have a chance to give birth each year, dependant on their weight.
        After birth, the animal loses weight in proportion to the weight of the new born.

        Returns
        -------
        new_born: Animal
            Either a herbivore or carnivore depending on the parent species
        """
        if self.weight >= self.weight_check_pregnancy():
            if random.random() < self.probability_pregnancy(n):
                if type(self).__name__ == 'Herbivore':
                    new_born = Herbivore()
                else:
                    new_born = Carnivore()

                self.birth_update_weight(new_born)
                return new_born
        return None

    def migrate(self, nearby_cells):
        """
        Animal chooses cell to migrate to out of the four adjacent cells.
        Animals move once per year.

        Parameters
        ----------
        nearby_cells: List
            List of the four adjacent cells

        Returns
        -------
        destination: Cell
            The cell the animal chooses to migrate to.
            Returns no cell if the chosen cell is inaccessible.
        """
        if random.random() < self.parameters['mu'] * self.fitness:
            destination = random.choice(nearby_cells)
            if destination.is_accessible:
                self.has_moved = True
                return destination
            else:
                return None


class Herbivore(Animals):
    """
    Subclass for herbivores.
    """

    parameters = {
        'w_birth': 8.0,
        'sigma_birth': 1.5,
        'beta': 0.9,
        'eta': 0.05,
        'a_half': 40.0,
        'phi_age': 0.2,
        'w_half': 10.0,
        'phi_weight': 0.1,
        'mu': 0.25,
        'gamma': 0.2,
        'zeta': 3.5,
        'xi': 1.2,
        'omega': 0.4,
        'F': 10.0,
        'DeltaPhiMax': None
    }

    def __init__(self, age=0, weight=None):
        """
        Herbivore initializer.
        """
        super().__init__(age, weight)

    def feed(self, cell_fodder_amount):
        """
        Herbivore consumes fodder.
        The amount it eats depends on the available amount in the cell.

        Parameters
        ----------
        cell_fodder_amount: float
            Amount of fodder in cell.

        Returns
        -------
        consumed: float
            Amount of consumed fodder in cell.
        """
        consumed = self.parameters['F']
        if cell_fodder_amount < consumed:
            consumed = cell_fodder_amount
            self.weight += self.weight_increase(consumed)
            self.update_fitness()
            return consumed
        else:
            self.weight += self.weight_increase(consumed)
            self.update_fitness()
            return consumed


class Carnivore(Animals):
    """
    Subclass for carnivores.
    """
    parameters = {
        'w_birth': 6.0,
        'sigma_birth': 1.0,
        'beta': 0.75,
        'eta': 0.125,
        'a_half': 60.0,
        'phi_age': 0.4,
        'w_half': 4.0,
        'phi_weight': 0.4,
        'mu': 0.4,
        'gamma': 0.8,
        'zeta': 3.5,
        'xi': 1.1,
        'omega': 0.9,
        'F': 50.0,
        'DeltaPhiMax': 10.0
    }

    def __init__(self, age=0, weight=None):
        """
        Carnivore initializer
        """
        super().__init__(age, weight)

    def fitness_higher_than_prey_fitness(self, prey):
        """
        Checks if carnivore fitness is between prey fitness and max fitness.

        Parameters
        ----------
        prey: Herbivore
            The herbivore the carnivore wants to kill.

        Returns
        -------
        bool
            True if the carnivore's fitness is within the given interval.
        """
        return 0 < self.fitness - prey.fitness <= self.parameters['DeltaPhiMax']

    def kill_probability(self, prey):
        """
        Calculates the probability that the carnivore will kill it's prey.

        Parameters
        ----------
        prey: Herbivore
            The herbivore the carnivore is attempting to kill.

        Returns
        -------
        float
            Probability of the carnivore killing it's prey
        """
        return (self.fitness - prey.fitness) / self.parameters['DeltaPhiMax']

    def kill(self, nearby_herbs):
        """
        The carnivore tries to kill an herbivore in the same cell.
        The carnivore continues to kill and eat nearby herbivores until
        it's full or there are no more nearby herbivores.
        The carnivore's weight is increased when it eats an herbivore.
        The carnivore's fitness is thus updated.

        Parameters
        ----------
        nearby_herbs: list
            List of herbivores in the same cell.

        Returns
        -------
        killed_herbs: list
            List of all herbivores killed by the carnivore.
        """
        kill_attempts = 1
        eaten = 0
        killed_herbs = []
        num_nearby_herbivores = len(nearby_herbs)

        for herb in nearby_herbs:
            if eaten < self.parameters['F'] and kill_attempts <= num_nearby_herbivores:
                if self.fitness <= herb.fitness:
                    probability = 0
                elif self.fitness_higher_than_prey_fitness(herb):
                    probability = self.kill_probability(herb)
                else:
                    probability = 1

                if random.random() < probability:
                    self.weight += self.weight_increase(herb.weight)
                    eaten += herb.weight
                    self.update_fitness()
                    killed_herbs.append(herb)

                kill_attempts += 1

        return killed_herbs
