#!python
# -*- coding: utf-8 -*-

__author__ = "Vetle Reinholt, Kim Næss Kynningsrud"
__email__ = "vetle.aasen.reinholt@nmbu.no, kim.nass.kynningsrud@nmbu.no"

from .cell import Lowland, Highland, Desert, Water
import numpy as np


class Island:
    """
    The island class. Superclass for all landscape types.
    """
    landscape_mapping = {
        'D': Desert,
        'W': Water,
        'H': Highland,
        'L': Lowland
    }

    def __init__(self, geogr, ini_pop):
        """
        Class constructor for Island.

        Parameters
        ----------
        geogr: str
            String depicting the island map. Defines its shape and landscape types.
        ini_pop: list
            List of all animals and their location to be initially added to the island.
        """
        self.geogr = geogr
        self.island = self.init_map(ini_pop)

    def count_animals(self):
        """
        Counts number of animals on the island separately.
        """
        return (sum(cell.get_num_herbivores() for cell in self.island.flatten()),
                sum(cell.get_num_carnivores() for cell in self.island.flatten()))

    def init_map(self, ini_pop):
        """
        Creates the island using the input island string.

        Parameters
        ----------
        ini_pop: list
            List of animals and their location to be initially added to the island.

        The input island string is split up, and an island is generated from it.
        The specified population is then added to the specified landscape cells of the island.
        """

        char_matrix = np.array([[char for char in row] for row in self.geogr.split()])
        island = np.empty(char_matrix.shape, dtype=object)
        for i, row in enumerate(char_matrix):
            for j, char in enumerate(row):
                _pop = None
                for _val in ini_pop:
                    if (i + 1, j + 1) == _val['loc']:
                        _pop = _val['pop']
                if char not in self.landscape_mapping:
                    raise ValueError('Invalid landscape')
                island[i, j] = self.landscape_mapping[char](_pop)

        return island

    def nearby_cells(self, row, col):
        """
        Returns list of the four adjacent(not diagonally) cells to the current cell.
        """
        nearby_cells = [self.island[row + 1, col], self.island[row, col + 1],
                        self.island[row - 1, col], self.island[row, col - 1]]
        return nearby_cells

    def adding_population(self, population):
        """
        Adds the specified population to the island.

        Parameters
        ----------
        population: list
            List of animals and their location to be initially added to the island.
        """
        for _elements in population:
            self.island[_elements['loc'][0] - 1, _elements['loc'][1] - 1].add_animals(
                _elements['pop'])

    def island_cycle(self):
        """
        Runs the annual cycle of the island for each separate landscape cell.
        """
        for i, row in enumerate(self.island):
            for j, cell in enumerate(row):
                if cell.is_accessible:
                    nearby_cells = self.nearby_cells(i, j)
                    cell.fodder_gen()
                    cell.feeding()
                    cell.procreation()
                    cell.migration(nearby_cells)
                    cell.aging_weightloss()
                    cell.death()
