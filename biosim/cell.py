#!python
# -*- coding: utf-8 -*-

__author__ = "Vetle Reinholt, Kim Næss Kynningsrud"
__email__ = "vetle.aasen.reinholt@nmbu.no, kim.nass.kynningsrud@nmbu.no"

from .animals import Carnivore, Herbivore


class Cell:
    """
    Superclass for the cell types.
    """
    parameters = {'f_max': 0}

    def __init__(self, init_pop=None):
        """
        Constructor for Cell class.

        Parameters
        ----------
        init_pop: list
            List of animals and their location to be initially added to the island.
        """
        self.herbivores = []
        self.carnivores = []
        self.add_animals(init_pop)
        self.fodder = self.parameters['f_max']

    def get_num_herbivores(self):
        """
        Returns the number of herbivores in the cell.
        """
        return len(self.herbivores)

    def get_num_carnivores(self):
        """
        Returns the number of carnivores in the cell.
        """
        return len(self.carnivores)

    def add_animals(self, population):
        """
        Adds animals to the cell. Works together with adding_population.

        Parameters
        ----------
        population: list
            List of animals and their location to be initially added to the island.
        """
        if population is not None:
            for animal in population:
                if animal['species'] == 'Herbivore':
                    self.herbivores.append(Herbivore(animal['age'], animal['weight']))
                elif animal['species'] == 'Carnivore':
                    self.carnivores.append(Carnivore(animal['age'], animal['weight']))

    @staticmethod
    def sort_herb(herbivores):
        """
        Sorts the list of herbivores by fitness in descending order.
        """
        sorted_herbivores = sorted(herbivores, key=lambda x: x.fitness)
        return sorted_herbivores

    @staticmethod
    def sort_carn(carnivores):
        """
        Sorts the list of carnivores by fitness in ascending order.
        """
        sorted_carnivores = sorted(carnivores, key=lambda x: x.fitness, reverse=True)
        return sorted_carnivores

    def feeding(self):
        """
        Animals eat.

        Fodder is generated first.
        Herbivores eat plant fodder before carnivores eat herbivores.
        The most fit carnivore eats first and preys upon the least fit herbivore.

        The amount of fodder is reduced when a herbivore eats.
        The number of herbivores in the cell is reduced when the carnivore kills.
        """
        sorted_herbivores = self.sort_herb([herb for herb in self.herbivores])
        sorted_carnivores = self.sort_carn([carn for carn in self.carnivores])
        self.fodder_gen()

        self.herbivores = sorted_herbivores
        self.carnivores = sorted_carnivores

        # herbivores eat
        for h in self.herbivores:
            consumed = h.feed(self.fodder)
            self.fodder -= consumed
            h.update_fitness()

        # carnivores eat
        nearby_herbivores = sorted_herbivores
        for c in self.carnivores:
            killed = c.kill(nearby_herbivores)
            nearby_herbivores = [herb for herb in nearby_herbivores if herb not in killed]
            c.update_fitness()

        self.herbivores = nearby_herbivores

    def procreation(self):
        """
        Each animal in a cell attempts to procreate once each year.
        The newborn animal is added to the cell's population.
        """
        if len(self.herbivores) + len(self.carnivores) != 0:
            def newborns(pop):
                num_animals = len(pop)
                return [nb for nb in [parent.birth(num_animals) for parent in pop]
                        if nb is not None]

            self.herbivores.extend(newborns(self.herbivores))
            self.carnivores.extend(newborns(self.carnivores))

    def migration(self, nearby_cells):
        """
        Each animal attempts to migrate if it hasn't already tried to migrate this year.
        The possibility of migration depends upon the animal's fitness.

        The destination cell is chosen at random between the four adjacent cells. After the
        destination is chosen, the animal will only move if the destination is accessible.

        The list of animals in the current cell is updated to not include the migrating animals.

        If the chosen cell is accessible, the animals move there.
        """
        migrations = []
        if len(self.herbivores) + len(self.carnivores) != 0:
            for animal in (self.carnivores + self.herbivores):
                if animal.has_moved is False:
                    destination = animal.migrate(nearby_cells)
                    if destination is not None:
                        migrations.append((animal, destination))
        migrating_animals = []
        for migration in migrations:
            migrating_animals.append(migration[0])

        # updates list of remaining animals in current cell
        self.carnivores = [carnivore for carnivore in self.carnivores if
                           carnivore not in migrating_animals]
        self.herbivores = [herbivore for herbivore in self.herbivores if
                           herbivore not in migrating_animals]

        # moves animals to their chosen cell
        for migration in migrations:
            if type(migration[0]) == Herbivore:
                migration[1].herbivores.append(migration[0])
            else:
                migration[1].carnivores.append(migration[0])

        for animal in (self.carnivores + self.herbivores):
            animal.has_moved = False

    def aging_weightloss(self):
        """
        Each year, the animal ages and loses weight.
        """
        for animal in (self.herbivores + self.carnivores):
            animal.aging()
            animal.weight_decrease()

    def death(self):
        """
        Each year, there is a probability that the animal will die depending on it's fitness.
        """
        if len(self.herbivores) + len(self.carnivores) != 0:
            def remaining_animals(pop):
                return [animals for animals in pop if not animals.probability_death()]

            self.carnivores = remaining_animals(self.carnivores)
            self.herbivores = remaining_animals(self.herbivores)

    def fodder_gen(self):
        """
        Amount of fodder in the cell is reset each year.
        """
        self.fodder = self.parameters['f_max']


class Lowland(Cell):
    """
    Subclass of cell.
    Lowland can be entered.
    Lowland has a lot of available food.
    """
    is_accessible = True
    parameters = {'f_max': 800.0}


class Highland(Cell):
    """
    Subclass of cell.
    Highland can be entered.
    Highland has some available food.
    """
    is_accessible = True
    parameters = {'f_max': 300.0}


class Water(Cell):
    """
    Subclass of cell.
    Water cannot be entered.
    Water has no available food.
    """
    is_accessible = False


class Desert(Cell):
    """
    Subclass of cell.
    Desert has no available food.
    """
    is_accessible = True
