#!python
# -*- coding: utf-8 -*-

__author__ = "Vetle Reinholt, Kim Næss Kynningsrud"
__email__ = "vetle.aasen.reinholt@nmbu.no, kim.nass.kynningsrud@nmbu.no"

import random

import numpy as np
from .graphics import Graphics
from .animals import Herbivore, Carnivore
from .cell import Highland, Lowland
from .island import Island


class BioSim:
    species_mapping = {
        'Herbivore': Herbivore,
        'Carnivore': Carnivore
    }

    def __init__(self,
                 island_map,
                 ini_pop=None,
                 seed=None,
                 ymax_animals=None,
                 cmax_animals=None,
                 hist_specs=None,
                 img_base=None,
                 img_fmt=None
                 ):
        """
        BioSim class constructor.

        :param island_map: Multi-line string specifying island geography
        :param ini_pop: List of dictionaries specifying initial population
        :param seed: Integer used as random number seed
        :param ymax_animals: Number specifying y-axis limit for graph showing animal numbers
        :param cmax_animals: Dict specifying color-code limits for animal densities
        :param hist_specs: Specifications for histograms
        :param img_base: String with beginning of file name for figures, including path
        :param img_fmt: String with file type for figures, e.g. 'png

        If ymax_animals is None, the y-axis limit should be adjusted automatically.

        If cmax_animals is None, sensible, fixed default values should be used.
        cmax_animals is a dict mapping species names to numbers, e.g.,
            {'Herbivore': 50, 'Carnivore': 20}

        hist_specs has not been implemented, yet.

        If img_base is None, no figures are written to file.
        Filename are formed as

            '{}_{:05d}.{}'.format(img_base, img_no, img_fmt)

        where img_no are consecutive image numbers starting from 0.
        img_base should contain a path and beginning of a file name.
        """

        random.seed(seed)
        self.simulated_island = Island(island_map, ini_pop)
        self._graphics = Graphics(img_base,
                                  img_fmt,
                                  ymax_animals,
                                  cmax_animals,
                                  hist_specs)
        self.island_map = island_map

        self._year = 0
        self._final_year = None

        self.num_cells = len(island_map)

    def simulate(self, num_years, vis_years=1, img_years=None):
        """
        Runs the simulation and visualizes the result.

        :param num_years: number of simulation years to execute
        :param vis_years: interval between visualization updates
        :param img_years: interval between visualizations saved to files
                            (default: vis_years)

        .. note:: Image files will be numbered consecutively.
        """
        if img_years is None:
            img_years = vis_years

        if img_years % vis_years != 0:
            raise ValueError('img_years must be multiple of vis_years')

        self._final_year = self._year + num_years
        self._graphics.setup(self._final_year, img_years)
        self._graphics.make_map(self.island_map)

        while self._year < self._final_year:
            self.simulated_island.island_cycle()
            self._year += 1

            if self._year % vis_years == 0:
                self._graphics.update(self._year,
                                      self.herb_matrix(),
                                      self.carn_matrix(),
                                      self.simulated_island.count_animals(),
                                      )

    @staticmethod
    def set_animal_parameters(species, params):
        """
        Sets parameters for different species.

        Parameters
        ----------
        species: str
            String with the name of the animal species
        params: dict
            Dictionary with parameters to be assigned to the given animal species
        """
        if species == 'Herbivore':
            for parameter in params:
                Herbivore.parameters[parameter] = params[parameter]
        elif species == 'Carnivore':
            for parameter in params:
                Carnivore.parameters[parameter] = params[parameter]
        else:
            raise ValueError

    @staticmethod
    def set_landscape_parameters(landscape, params):
        """
        Sets parameters for different landscape types.

        Parameters
        ----------
        landscape: str
            Letter indicating the type of landscape
        params: dict
            Dictionary with parameters to be assigned to the given landscape type.
        """
        if landscape == 'H':
            for parameter in params:
                Highland.parameters[parameter] = params[parameter]
        elif landscape == 'L':
            for parameter in params:
                Lowland.parameters[parameter] = params[parameter]
        else:
            raise ValueError

    def add_population(self, population):
        """
        Adds a population to the island.

        Parameters
        ----------
        population: list
            List of the animals to be added
        """
        self.simulated_island.adding_population(population)

    @property
    def year(self):
        """
        Last year simulated.
        """
        return self._year

    @property
    def num_animals(self):
        """
        Total number of animals on island.
        """
        return sum(self.simulated_island.count_animals())

    @property
    def num_animals_per_species(self):
        """
        Number of animals per species in island, as dictionary.
        """

        number_of_herbivores = self.simulated_island.count_animals()[0]
        number_of_carnivores = self.simulated_island.count_animals()[1]

        return {'Herbivore': number_of_herbivores,
                'Carnivore': number_of_carnivores}

    def make_movie(self, movie_fmt=None):
        """
        Creates MPEG4 movie from visualization images saved.
        """

        self._graphics.make_movie(movie_fmt)

    def herb_matrix(self):
        """
        Matrix of herbivores in each cell on the island.
        """
        char_matrix = np.array([[char for char in row] for row in self.island_map.split()])
        herb_map = np.zeros(char_matrix.shape, dtype=int)
        for i, row in enumerate(self.simulated_island.island):
            for j, cell in enumerate(row):
                herb_map[i, j] = cell.get_num_herbivores()
        return herb_map

    def carn_matrix(self):
        """
        Matrix of carnivores in each cell on the island.
        """
        char_matrix = np.array([[char for char in row] for row in self.island_map.split()])
        carn_map = np.zeros(char_matrix.shape, dtype=int)
        for i, row in enumerate(self.simulated_island.island):
            for j, cell in enumerate(row):
                carn_map[i, j] = cell.get_num_carnivores()
        return carn_map
